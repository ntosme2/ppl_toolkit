#!/usr/bin/env python3

from urllib import request
import numpy as np
import os
import time

# altitude_meter = [-610, 11000, 20000, 32000, 47000, 51000, 71000, 84852]
# altitude_feet = [i * 3.28084 for i in altitude_meter]
# pressure_Pascal = [108900, 22632, 5474.9, 868.02, 110.91, 66.939, 3.9564, 0.3734]
# pressure_millibar = [i/100 for i in pressure_Pascal]

# today = date.today()
# cycle = '00'
# resolution = '1p00'
# hour = '000'
# link = 'https://nomads.ncep.noaa.gov/pub/data/nccf/com/gfs/prod/gfs.{}{}{}/{}/gfs.t{}z.pgrb2.{}.f{}'.format(today.year, today.month, today.day, period)

cache_file = '/tmp/wind.cache'
cache_seconds = 10*60
wind_altitudes = [0, 3000, 6000, 9000, 12000, 18000, 24000, 30000, 34000, 39000]

# https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2009JD013493
standard_lapse_rate = 1.5 # deg C / 1000 ft

class Wind:
    def __init__(self):
        link = 'https://aviationweather.gov/windtemp/data?level=low&fcst=06&region=all&layout=off&date='
        if os.path.isfile(cache_file) and (time.time() - os.path.getmtime(cache_file)) < cache_seconds:
            print('Cached wind data age is < {} minutes, using cache.'.format(cache_seconds/60))
        else:
            print('Fetching wind data:', link)
            try:
                with request.urlopen(link) as f:
                    data = f.read()
                    with open('/tmp/wind.cache', 'wb') as wf:
                        wf.write(data)
            except:
                print('Error pulling current wind data, using cache!')
        print()

        first = b'FT  3000    6000    9000   12000   18000   24000  30000  34000  39000'
        self.airports = {}

        skip = True
        with open(cache_file, 'rb') as f:
            data = f.read()
            class Pt: pass

            for x in data.split(b'\n'):
                if x == first:
                    skip = False
                    continue
                if len(x) == 69 and not skip:
                    p = Pt()
                    ident = x[:3]
                    #          hdg       kts      temp
                    w3  = (x[4:6],   x[6:8],   b'')
                    w6  = (x[9:11], x[11:13], x[13:16])
                    w9  = (x[17:19], x[19:21], x[21:24])
                    w12 = (x[25:27], x[27:29], x[29:32])
                    w18 = (x[33:35], x[35:37], x[37:40])
                    w24 = (x[41:43], x[43:45], x[45:48])
                    w30 = (x[49:51], x[51:53], x[53:55])
                    w34 = (x[56:58], x[58:60], x[60:62])
                    w39 = (x[63:65], x[65:67], x[67:69])

                    def toInt(w):
                        ret = [0.000001,0.000001,0.0]
                        if w[0].strip() == b'':
                            ret = [-999, -999, -999]
                            return ret
                        ret[0] = int(w[0])*10
                        ret[1] = int(w[1])
                        if w[2].strip() == b'':
                            ret[2] = -999
                        else:
                            ret[2] = int(w[2])
                        # 9900 means calm/variable
                        if ret[0] == 990:
                            ret[0] = 0
                        # handle winds > 99 kts
                        if ret[0] > 360:
                            ret[0] -= 500
                            ret[1] += 100
                        return ret

                    def est_temp(temp, delta):
                        return temp - standard_lapse_rate*delta

                    w39 = toInt(w39)
                    w34 = toInt(w34)
                    w30 = toInt(w30)
                    w24 = toInt(w24)
                    w18 = toInt(w18)
                    w12 = toInt(w12)
                    w9  = toInt(w9)
                    w6  = toInt(w6)
                    w3  = toInt(w3)
                    w0 = [-999, w3[1], w3[2]]

                    p.hdg   = [x[0] for x in [w0, w3, w6, w9, w12, w18, w24, w30, w34, w39]]
                    p.speed = [x[1] for x in [w0, w3, w6, w9, w12, w18, w24, w30, w34, w39]]
                    p.temp  = [x[2] for x in [w0, w3, w6, w9, w12, w18, w24, w30, w34, w39]]
                    # extrapolate data to sea level
                    for i in reversed(range(len(wind_altitudes)-1)):
                        if p.hdg[i] == -999:
                            p.hdg[i] = p.hdg[i+1]
                        if p.speed[i] == -999:
                            p.speed[i] = p.speed[i+1]
                        if p.temp[i] == -999:
                            delta = wind_altitudes[i+1] - wind_altitudes[i]
                            p.temp[i] = round(p.temp[i+1] + standard_lapse_rate*delta/1000)

                    self.airports[ident] = p


def windCorrection(wind, course, tas, alt):
    w_dir = np.interp(alt, wind_altitudes, wind.hdg)
    w_speed = np.interp(alt, wind_altitudes, wind.speed)
    w_temp = np.interp(alt, wind_altitudes, wind.temp)

    # change wind vector FROM -> TO
    w_dir_to = w_dir + 180
    if w_dir_to >= 360:
        w_dir_to -= 360

    # determine sign of wind correction angle
    wc_sign = course-w_dir
    if wc_sign > 180:
        wc_sign -= 360
    if wc_sign < -180:
        wc_sign += 360
    if wc_sign < 0:
        wc_sign = 1
    else:
        wc_sign = -1

    # calculate ground speed
    A = course - w_dir_to
    b = -2*w_speed*np.cos(np.radians(A))
    c = w_speed**2 - tas**2
    gs = max((-b + np.sqrt(b**2 - 4*c))/2, (-b - np.sqrt(b**2 - 4*c))/2)

    # calculate wind correction angle
    wta = np.radians(course - w_dir_to)
    wca = wc_sign * np.arccos((tas**2 + gs**2 - w_speed**2)/(2*tas*gs))
    hdg = course + np.degrees(wca)
    if hdg < 0:
        hdg += 360

    return w_dir, w_speed, np.degrees(wca), hdg, gs, w_temp

if __name__ == '__main__':
    w = Wind()
