#!/usr/bin/env python3

import numpy

# inhg_to_pascals = 1/0.00029529980164712
# Pb = 29.92*inhg_to_pascals
# T0 = 273.15 # K (Celsius to Kelvin)
# Tb = 15 + T0 # K (standard temperature)
# Lb = 0.005 # K/m

alt_ft = [0, 36089, 65617, 104987, 154199, 167323, 232940]
pressure_inhg = [29.92126, 6.683245, 1.616734, 0.2563258, 0.0327506, 0.01976704, 0.00116833]

altitude = 12500
EAS = 147
TAS = EAS*numpy.sqrt(pressure_inhg[0]/numpy.interp(altitude, alt_ft, pressure_inhg))
print('altitude', altitude)
print('EAS', EAS)
print('TAS', TAS)


