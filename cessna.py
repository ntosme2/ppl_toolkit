import numpy as np
from airspeed import tas_to_cas

class C172N:
    def __init__(self):
        # fpm, gph, knots
        self.climb = 700, 11
        self.cruise = 0, 8 # 2400 RPM
        self.descent = 500, 6
        self.fuel_usable = 40
