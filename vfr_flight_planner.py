#!/usr/bin/env python3.6
import collections
from geopy.distance import distance
import math
from datetime import date
import yaml
import sys
# include path for WMM wrapper
sys.path.append('build')
import wmm

# NCEP Wind Aloft Forecast
from wind import Wind, windCorrection
from airspeed import cas_to_tas, tas_to_cas

import numpy as np

def format_time(hours):
    h = int(hours)
    m = int(60*(hours - h))
    s = int(3600*(hours - h - m/60.))
    out = '{:02d}s'.format(s)
    if m:
        out = '{:02d}m{}'.format(m, out)
    if h:
        out = '{}h{}'.format(h, out)
    return out

def get_distance(pt1, pt2):
    return distance((pt1.latitude, pt1.longitude), (pt2.latitude, pt2.longitude)).nm

def get_course(pt1, pt2):
    lat1 = np.deg2rad(pt1.latitude)
    lon1 = np.deg2rad(pt1.longitude)
    lat2 = np.deg2rad(pt2.latitude)
    lon2 = np.deg2rad(pt2.longitude)
    dlon = lon2 - lon1
    y = math.sin(dlon) * math.cos(lat2)
    x = math.cos(lat1) * math.sin(lat2) - math.sin(lat1) * math.cos(lat2) * math.cos(dlon)
    course = np.rad2deg(math.atan2(y, x))
    if course < 0:
        course += 360
    return course

def get_mag_bearing(course, pt):
    mag_var = wmm.get_magnetic_variation('WMM2015v2COF/WMM.COF', pt.latitude, pt.longitude, 0, date.today().year)
    brng = course - mag_var
    if brng < 0:
        brng += 360
    return brng, mag_var

# TODO
def mag_dev(hdg):
    c_dev = 0
    return hdg, c_dev

# uses data from FAA CIFP; download at
# https://www.faa.gov/air_traffic/flight_info/aeronav/digital_products/cifp/download/

class Airport:
    lookup = {
            'ident': (6,10),
            'longest_runway': (27,30),
            'latitude': (32,41),
            'longitude': (41,51),
            'mag_var': (51,56),
            'elevation': (56,61),
            'name': (93,123),
            'pub_mil': (80,81),
            }

    def match(line):
        if not line[0:6] == b'SUSAP ':
            return False
        if not (line[10] == ord(b'K') and line[12] == ord(b'A')):
            return False
        return True

    def __init__(self, line):
        for k,(a,b) in Airport.lookup.items():
            setattr(self, k, line[a:b])

        self.name = self.name.strip()
        self.ident = self.ident.strip()
        self.weather = None
        self.runways = []

        # latitude is in {N,S}HHMMSS.SS, convert to decimal
        lat = float(self.latitude[1:3]) + float(self.latitude[3:5])/60 + float(self.latitude[5:])/360000
        if self.latitude[0] == ord(b'S'):
            lat = -lat
        self.latitude = lat

        # longitude is in {E,W}0HHMMSS.SS, convert to decimal
        lon = float(self.longitude[1:4]) + float(self.longitude[4:6])/60 + float(self.longitude[6:])/360000
        if self.longitude[0] == ord(b'W'):
            lon = -lon
        self.longitude = lon

        mvar = float(self.mag_var[1:])/10
        if self.mag_var[0] == ord(b'E'):
            mvar = -mvar
        self.mag_var = mvar
        self.elevation = float(self.elevation.strip())

# load flight plan from argv[1] in YAML format
if len(sys.argv) > 1:
    with open(sys.argv[1], 'r') as f:
        flight = yaml.safe_load(f.read())
        route = flight['route']
        TAS = flight['TAS']
        altitude = flight['altitude']
        user_waypoints = []
        if 'user_waypoints' in flight:
            class WP: pass
            for w in flight['user_waypoints']:
                uwp = WP()
                uwp.waypoint = True
                # use cruise altitude for waypoint elevation to get correct fuel burn
                uwp.elevation = altitude
                uwp.ident = w['ident'].encode()
                uwp.latitude = w['latitude']
                uwp.longitude = w['longitude']
                user_waypoints.append(uwp)
        manufacturer = __import__(flight['aircraft'][0])
        aircraft = getattr(manufacturer, flight['aircraft'][1])()

else:
    print('usage: {} <flight.yaml>'.format(sys.argv[0]))
    raise SystemExit

wind = Wind()

airports = []
with open('CIFP/CIFP_201913/FAACIFP18', 'rb') as f:
    for line in f.readlines():
        data = line.strip()
        if len(data) == 132:
            if Airport.match(line):
                a = Airport(line)
                for w, wa in wind.airports.items():
                    if a.ident == (b'K' + w):
                        a.weather = wa
                airports.append(a)

# ICAO identifier dict
airport_dict = {}
for apt in airports:
    airport_dict[apt.ident] = apt

def get_txt_field(line, offset, length):
    return line[offset-1:offset+length-1].strip()

# extract frequencies from NASR data
with open('NASR/APT.txt', 'rb') as f:
    airport_dict_site_number = {}
    for line in f.readlines():
        data = line.strip()
        if data.startswith(b'APT'):
            ident = get_txt_field(data, 1211, 7)
            if ident in airport_dict:
                airport_dict[ident].ident_us = get_txt_field(data, 28, 4)
                airport_dict[ident].unicom = get_txt_field(data, 982, 7).decode()
                airport_dict[ident].ctaf = get_txt_field(data, 989, 7).decode()
                airport_dict[ident].site_number = get_txt_field(data, 4, 11)
                airport_dict_site_number[airport_dict[ident].site_number] = airport_dict[ident]
        if data.startswith(b'RWY'):
            site_number = get_txt_field(data, 4, 11)
            if site_number in airport_dict_site_number:
                rwy = {}
                rwy['id'] = get_txt_field(data, 17, 7).decode()
                rwy['length'] = int(get_txt_field(data, 24, 5))
                rwy['width'] = int(get_txt_field(data, 29, 4))
                rwy['right traffic'] = get_txt_field(data, 82, 1).decode()
                airport_dict_site_number[site_number].runways.append(rwy)

# US identifier dict
airport_dict_us = {}
for apt in airports:
    if hasattr(apt, 'ident_us'):
        airport_dict_us[apt.ident_us] = apt

with open('NASR/AWOS.txt', 'rb') as f:
    for line in f.readlines():
        data = line.strip()
        if data:
            ident_us = get_txt_field(data, 6, 4)
            if ident_us in airport_dict_us:
                awos_type = get_txt_field(data, 10, 10).decode()
                airport_dict_us[ident_us].awos = '{} ({})'.format(get_txt_field(data, 69, 7).decode(), awos_type)

spacings = {
        'FROM':  (15, '{: >{n}}'),
        'TO':    (15, '{: >{n}}'),
        'WIND':  (13, '{: >{n}}'),
        'THDG':  (5, '{: >{n}.0f}'),
        'MHDG':  (5, '{: >{n}.0f}'),
        'MVAR':  (5, '{: >{n}.1f}'),
        'CDEV':  (5, '{: >{n}.1f}'),
        'CHDG':  (5, '{: >{n}.0f}'),
        'TCRS':  (5, '{: >{n}.0f}'),
        'DST':   (8, '{: >{n}.1f}'),
        'LAT':   (10, '{: >{n}.5f}'),
        'LON':   (11, '{: >{n}.5f}'),
        'ALT':   (6, '{: >{n}.0f}'),
        'TEMP':  (5, '{: >{n}.0f}'),
        'CAS':   (4, '{: >{n}.0f}'),
        'WCA':   (4, '{: >{n}.0f}'),
        'GS':    (4, '{: >{n}.0f}'),
        'ETE':   (10, '{: >{n}}'),
        'ETA':   (10, '{: >{n}}'),
        'FUSED': (6, '{: >{n}}'),
        'FREM':  (5, '{: >{n}}'),
        'TCLIMB': (10, '{: >{n}}'),
        'TCRUISE': (10, '{: >{n}}'),
        'TDESCENT': (10, '{: >{n}}'),
        }
# out_order = ['FROM', 'TO', 'ALT', 'WIND', 'TEMP', 'CAS', 'TCRS', 'WCA', 'THDG', 'MVAR', 'MHDG', 'CDEV', 'CHDG', 'DST', 'GS', 'ETE', 'ETA', 'FUSED', 'FREM']
out_order = ['FROM', 'TO', 'WIND', 'ALT', 'CAS', 'GS', 'CHDG', 'GS', 'DST', 'ETE']
print(''.join(['{: >{n}}'.format(v, n=spacings[v][0]) for v in out_order]))
print('=' * sum([v[0] for k,v in spacings.items() if k in out_order]))

ETE_total = 0
DST_total = 0
fuel_burn = 0

def maybe_print_attr(obj, f):
    if hasattr(obj, f):
        print('{}: {}'.format(f, getattr(obj, f)))

def print_station_info(s):
    if not hasattr(s, 'waypoint'):
        print()
        print('### {} ###'.format(s.ident.decode()))
        maybe_print_attr(s, 'unicom')
        maybe_print_attr(s, 'ctaf')
        maybe_print_attr(s, 'awos')
        if s.runways:
            print('Runways:')
            for rwy in s.runways:
                print('  {}: LxW={}x{} {}'.format(rwy['id'], rwy['length'], rwy['width'], 'RT' if rwy['right traffic'] == 'Y' else ''))

apt_info = collections.OrderedDict()

for i in range(len(route)):
    a = None
    b = None
    for apt in user_waypoints + airports:
        if apt.ident == route[i-1].encode():
            a = apt
        if apt.ident == route[i].encode():
            b = apt
    if a is None:
        print('error: unknown identifier {}'.format(route[i-1]))
        break
    if b is None:
        print('error: unknown identifier {}'.format(route[i]))
        break

    if i == 0:
        apt_info[a.ident] = a
        continue

    # find the closest airport with winds aloft data
    closest_wx = [None, 999999]
    for apt in airports:
        dst = get_distance(a, apt)
        if apt.weather and (dst < closest_wx[1]):
            closest_wx = [apt, dst]
    wx = closest_wx[0].weather

    dist = get_distance(a, b)
    course = get_course(a, b)
    CAS = tas_to_cas(TAS, altitude)
    w_dir, w_speed, wca, true_hdg, gndspeed, w_temp = windCorrection(wx, course, TAS, altitude)
    mag_hdg, mag_var = get_mag_bearing(true_hdg, a)
    cmag_hdg, c_dev = mag_dev(mag_hdg)
    ETE = dist / gndspeed
    ETE_total += ETE
    DST_total += dist
    # print(a.elevation, b.elevation)
    climb_t = (altitude-a.elevation)/aircraft.climb[0]/60
    descent_t = (altitude-b.elevation)/aircraft.descent[0]/60
    cruise_t = ETE - climb_t - descent_t
    if cruise_t < 0:
        cruise_t = 0
        print('Warning: ETE too short to reach cruise altitude')
    fuel_burn += climb_t * aircraft.climb[1]
    fuel_burn += cruise_t * aircraft.cruise[1]
    fuel_burn += descent_t * aircraft.descent[1]
    # print(climb_t, cruise_t, descent_t)
    # print(climb_t*aircraft.climb[1], cruise_t*aircraft.cruise[1], descent_t*aircraft.descent[1])

    # print(ETE)

    strs = {
            'TCRS': course,
            'ALT': altitude,
            'WIND': '{:.0f}@{:.0f}:{}'.format(w_dir, w_speed, closest_wx[0].ident.decode()),
            'TEMP': w_temp,
            'CAS': CAS,
            'WCA': wca,
            'THDG': true_hdg,
            'MVAR': mag_var,
            'MHDG': mag_hdg,
            'CDEV': c_dev,
            'CHDG': cmag_hdg,
            'FROM': route[i-1],
            'TO': route[i],
            'DST': dist,
            'GS': gndspeed,
            'ETE': format_time(ETE),
            'ETA': 0,
            'FUSED': 0,
            'FREM': 0,
            'TCLIMB': format_time(climb_t),
            'TCRUISE': format_time(cruise_t),
            'TDESCENT': format_time(descent_t),
            # 'LAT': b.latitude,
            # 'LON': b.longitude
            }
    print(''.join([spacings[v][1].format(strs[v], n=spacings[v][0]) for v in out_order]))
    apt_info[b.ident] = b

for f in apt_info.values():
    print_station_info(f)
# add 45-min cruise fuel reserve
fuel_burn += 0.75 * aircraft.cruise[1]

print()
print('Required Fuel w/45m reserve (gal): {:.1f}'.format(fuel_burn))
print('ETE Total', format_time(ETE_total))
print('DST Total {:.1f} nm'.format(DST_total))

