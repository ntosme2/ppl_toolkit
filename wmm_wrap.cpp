#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

extern "C" {
#include "wrap_wrap.h"
}

PYBIND11_MODULE(wmm, m) {
    m.doc() = "wmm";
    m.def("get_magnetic_variation", &get_magnetic_variation, "");
}
