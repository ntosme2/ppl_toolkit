import numpy as np

alt_ft = [0, 36089, 65617, 104987, 154199, 167323, 232940]
pressure_inhg = [29.92126, 6.683245, 1.616734, 0.2563258, 0.0327506, 0.01976704, 0.00116833]
def cas_to_tas(CAS, altitude):
    return CAS*np.sqrt(pressure_inhg[0]/np.interp(altitude, alt_ft, pressure_inhg))

def tas_to_cas(TAS, altitude):
    return TAS/np.sqrt(pressure_inhg[0]/np.interp(altitude, alt_ft, pressure_inhg))

