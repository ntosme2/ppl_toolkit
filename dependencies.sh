# python-in-browser
wget https://github.com/brython-dev/brython/releases/download/3.8.0/Brython-3.8.0.tar.bz2
tar xf Brython-3.8.0.tar.bz2

# FAA airport/waypoint database
wget https://aeronav.faa.gov/Upload_313-d/cifp/CIFP_191205.zip
pushd .
mkdir -p CIFP
cd CIFP
unzip ../CIFP*.zip
popd

# FAA NASR database
wget https://nfdc.faa.gov/webContent/28DaySub/28DaySubscription_Effective_2020-04-23.zip
pushd .
mkdir -p NASR
cd NASR
unzip ../28DaySubscription*.zip
popd

# World Magnetic Model coefficients
wget https://www.ngdc.noaa.gov/geomag/WMM/data/WMM2015/WMM2015v2COF.zip
unzip WMM2015v2COF.zip

# World Magnetic Model Linux API
wget https://www.ngdc.noaa.gov/geomag/WMM/data/WMM2015/WMM2015v2_Linux.tar.gz
tar xf WMM2015v2_Linux.tar.gz
pushd .
cd WMM2015_Linux/src/
ln -s ../../WMM2015v2COF/WMM.COF .
popd

# Ubuntu APT dependencies
sudo apt install python3-numpy python3-geopy build-essential cmake python3-distutils python3-dev

#wget https://www.ftp.cpc.ncep.noaa.gov/wd51we/wgrib2/wgrib2.tgz
#tar xf wgrib2.tgz
