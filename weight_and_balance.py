
from browser import document, html
import math

canvas = document['zone8']
ctx = canvas.getContext('2d')
c_xmin = 120
c_xmax = 205
c_ymin = 1450
c_ymax = 2250
c_border = 25


gasoline_cte = 0.000950
gasoline_lbs_per_gal_60f = 6.09215
gasoline_std_temp_c = 15.5

cg_min = 81
cg_max = 92.5
normal_envelope = [
        (122, 1500),
        (144, 1775),
        (189, 2200),
        (203, 2200),
        (139, 1500),
        ]
utility_envelope = [
        (122, 1500),
        (144, 1775),
        (152, 1850),
        (159, 1850),
        (129, 1500),
        ]

def strfmt(val):
    return '{:0.2f}'.format(val)

x_range = c_xmax-c_xmin
y_range = c_ymax-c_ymin
x_center = (c_xmax+c_xmin)/2
y_center = (c_ymax+c_ymin)/2
x_scale = x_range / (canvas.width - 2*c_border)
y_scale = y_range / (canvas.height - 2*c_border)
cx_center = canvas.width/2
cy_center = canvas.height/2

def getXY(x, y):
    return cx_center + (x-x_center)/x_scale, cy_center - (y-y_center)/y_scale

def draw(event):
    ctx.clearRect(0, 0, canvas.width, canvas.height)

    ctx.strokeStyle = 'black'
    for e in [normal_envelope, utility_envelope]:
        ctx.beginPath()
        for i, (x,y) in enumerate(e):
            if not i:
                ctx.moveTo(*getXY(x, y))
            else:
                ctx.lineTo(*getXY(x, y))
        ctx.closePath()
        ctx.stroke()
    ctx.font = '10px serif'
    ctx.textAlign = 'center'
    ctx.textBaseline = 'middle'
    ctx.fillText(c_xmin, c_border, canvas.height-c_border/2)
    ctx.fillText(c_xmax, canvas.width-c_border, canvas.height-c_border/2)
    ctx.fillText(c_ymin, c_border/2, canvas.height-c_border)
    ctx.fillText(c_ymax, c_border/2, c_border)

    ctx.strokeStyle = 'green'
    ctx.beginPath()
    moment = float(document['m6'].value)/1000
    weight = float(document['w6'].value)
    x, y = getXY(moment, weight)
    ctx.arc(x, y, 6, 0, 2 * 3.1415927)
    ctx.stroke()

    ctx.strokeStyle = 'red'
    ctx.beginPath()
    moment = float(document['m7'].value)/1000
    weight = float(document['w7'].value)
    x, y = getXY(moment, weight)
    ctx.arc(x, y, 6, 0, 2 * 3.1415927)
    ctx.stroke()

def updateRow(w, a, m):
    ww = 0
    aa = 0
    for w0 in w:
        try:
            ww += float(document[w0].value)
        except LookupError:
            ww = 0
    try:
        aa = float(document[a].value)
    except LookupError:
        aa = 0
    document[m].value = strfmt(ww*aa)

def getSum(ids):
    ret = 0
    for x in ids:
        try:
            ret += float(document[x].value)
        except ValueError:
            return 0
    return ret

def calc(event):
    updateRow(['w0'], 'a0', 'm0')
    updateRow(['w1a', 'w1b'], 'a1', 'm1')
    updateRow(['w2a', 'w2b'], 'a2', 'm2')
    updateRow(['w3'], 'a3', 'm3')
    updateRow(['w4'], 'a4', 'm4')
    try:
        fuel = float(document['fuel'].value)
    except LookupError:
        fuel = 0
    try:
        temp = float(document['temp'].value)
    except LookupError:
        temp = gasoline_std_temp_c
    ratio = 1 - gasoline_cte*(temp-gasoline_std_temp_c)
    document['w5'].value = '{:0.2f}'.format(gasoline_lbs_per_gal_60f*ratio*fuel)
    updateRow(['w5'], 'a5', 'm5')
    tw = getSum(['w0', 'w1a', 'w1b', 'w2a', 'w2b', 'w3', 'w4', 'w5'])
    tm = getSum(['m0', 'm1', 'm2', 'm3', 'm4', 'm5'])
    ta = tm/tw
    document['w6'].value = strfmt(tw)
    document['a6'].value = strfmt(ta)
    document['m6'].value = strfmt(tm)
    tw = getSum(['w0', 'w1a', 'w1b', 'w2a', 'w2b', 'w3', 'w4'])
    tm = getSum(['m0', 'm1', 'm2', 'm3', 'm4'])
    ta = tm/tw
    document['w7'].value = strfmt(tw)
    document['a7'].value = strfmt(ta)
    document['m7'].value = strfmt(tm)
    draw(event)

for x in ['w0', 'w1a', 'w1b', 'w2a', 'w2b', 'w3', 'w4', 'w5', 'fuel', 'temp']:
    document[x].bind('keypress', calc)
    document[x].bind('focusout', calc)
